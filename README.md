# Build

```shell
mvn clean package
```

# Run

```shell
java -jar target/shortest-path-0.0.1-SNAPSHOT.jar
```