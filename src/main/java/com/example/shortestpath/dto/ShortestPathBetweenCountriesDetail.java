package com.example.shortestpath.dto;

import java.util.List;
import lombok.Getter;

@Getter
public class ShortestPathBetweenCountriesDetail {

    private final List<String> route;

    public ShortestPathBetweenCountriesDetail(List<String> route) {
        this.route = List.copyOf(route);
    }

}
