package com.example.shortestpath.controller;

import com.example.shortestpath.dto.ShortestPathBetweenCountriesDetail;
import com.example.shortestpath.service.ShortestPathBetweenCountriesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ShortestPathBetweenCountriesController {

    private static final Logger LOG = LoggerFactory.getLogger(ShortestPathBetweenCountriesController.class);

    private final ShortestPathBetweenCountriesService service;

    public ShortestPathBetweenCountriesController(ShortestPathBetweenCountriesService service) {
        this.service = service;
    }

    @GetMapping(value = "/routing/{origin}/{destination}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public ShortestPathBetweenCountriesDetail getShortestPathBetweenCountries(
            @PathVariable(value = "origin") String origin,
            @PathVariable(value = "destination") String destination
    ) {
        LOG.debug("getShortestPathBetweenCountries(origin={}, destination={})", origin, destination);
        return service.getShortestPathBetweenCountries(origin, destination);
    }

}
