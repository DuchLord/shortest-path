package com.example.shortestpath.model;

import com.google.gson.annotations.SerializedName;
import java.util.Set;
import lombok.Getter;

@Getter
public class Country {

    @SerializedName("cca3")
    private final String name;

    @SerializedName("borders")
    private final Set<String> neighborCountriesName;

    public Country(String name, Set<String> neighborCountriesName) {
        this.name = name;
        this.neighborCountriesName = Set.copyOf(neighborCountriesName);
    }

}
