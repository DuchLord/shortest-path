package com.example.shortestpath.service;

import com.example.shortestpath.dto.ShortestPathBetweenCountriesDetail;
import com.example.shortestpath.exception.CountryNotFoundException;
import com.example.shortestpath.exception.NoLandCrossingException;
import com.example.shortestpath.model.Country;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

@Service
public class ShortestPathBetweenCountriesService {

    private static final Logger LOG = LoggerFactory.getLogger(ShortestPathBetweenCountriesService.class);
    private static final Gson GSON = new GsonBuilder().create();

    private static final String COUNTRIES_SOURCE_URL = "https://raw.githubusercontent.com/mledoze/countries/master/countries.json";

    public ShortestPathBetweenCountriesDetail getShortestPathBetweenCountries(String origin, String destination) {
        LOG.debug("getShortestPathBetweenCountries(origin={}, destination={})", origin, destination);
        final Map<String, Country> countryGraph = getCountriesMap();
        final Country originCountry = countryGraph.computeIfAbsent(origin, name -> {
            throw new CountryNotFoundException(name);
        });
        final Country destinationCountry = countryGraph.computeIfAbsent(destination, name -> {
            throw new CountryNotFoundException(name);
        });
        final List<String> shortestPath = searchShortestPath(originCountry, destinationCountry, countryGraph);
        if (shortestPath.size() <= 2) {
            // If there is no land crossing, throw exception
            throw new NoLandCrossingException();
        }
        LOG.debug("Shortest path: {}", shortestPath);
        return new ShortestPathBetweenCountriesDetail(shortestPath);
    }

    private List<String> searchShortestPath(Country start, Country goal, Map<String, Country> countryGraph) {
        final Queue<Country> queue = new ArrayDeque<>();
        final Set<Country> visited = new HashSet<>();
        final Map<String, String> pathBetweenCountries = new HashMap<>();
        final List<String> shortestPath = new LinkedList<>();

        queue.add(start);
        while (!queue.isEmpty()) {
            final Country currentCountry = queue.remove();
            if (currentCountry.getName().equals(goal.getName())) {
                String countryName = goal.getName();
                while (!Objects.equals(countryName, start.getName())) {
                    shortestPath.add(countryName);
                    countryName = pathBetweenCountries.get(countryName);
                }
                shortestPath.add(start.getName());
                Collections.reverse(shortestPath);
                return shortestPath;
            } else {
                visited.add(currentCountry);
                currentCountry.getNeighborCountriesName().forEach(countryName -> {
                    if (!pathBetweenCountries.containsKey(countryName)) {
                        pathBetweenCountries.put(countryName, currentCountry.getName());
                    }
                });
                queue.addAll(currentCountry.getNeighborCountriesName().stream().map(countryGraph::get).collect(Collectors.toSet()));
                queue.removeAll(visited);
            }
        }
        return Collections.emptyList();
    }

    private Map<String, Country> getCountriesMap() {
        final Set<Country> countries;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(COUNTRIES_SOURCE_URL).openStream(), StandardCharsets.UTF_8))) {
            countries = GSON.fromJson(reader, new TypeToken<Set<Country>>() {
            }.getType());
        } catch (IOException e) {
            throw new ResourceAccessException("Unable to load resource json.", e);
        }
        return countries.stream().collect(Collectors.toMap(Country::getName, country -> country));
    }

}
