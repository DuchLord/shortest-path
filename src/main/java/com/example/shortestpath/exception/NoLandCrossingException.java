package com.example.shortestpath.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoLandCrossingException extends RuntimeException {

    public NoLandCrossingException() {
        super("No land crossing.");
    }
}
